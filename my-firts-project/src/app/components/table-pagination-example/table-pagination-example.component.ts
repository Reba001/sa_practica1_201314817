import {AfterViewInit, Component, ViewChild} from '@angular/core';
import {MatPaginator, MatPaginatorModule} from '@angular/material/paginator';
import {MatTableDataSource, MatTableModule} from '@angular/material/table';
import {PageEvent} from '@angular/material/paginator';
import {JsonPipe} from '@angular/common';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {FormsModule} from '@angular/forms';
import {MatInputModule} from '@angular/material/input';
import {MatFormFieldModule} from '@angular/material/form-field';
import { PokemonService } from 'src/app/controllers/pokemon.service';
import { Pokemon } from 'src/app/models/pokemon';
import {MatTabsModule} from '@angular/material/tabs';
import {MatDividerModule} from '@angular/material/divider';
import {MatIconModule} from '@angular/material/icon';
import { Autor, AutorRespuesta } from 'src/app/models/autor';
import { MatButtonModule } from '@angular/material/button';

@Component({
  selector: 'app-table-pagination-example',
  templateUrl: './table-pagination-example.component.html',
  styleUrls: ['./table-pagination-example.component.css'],
  standalone: true,
  imports: [MatTableModule, MatFormFieldModule,
    MatInputModule,
    FormsModule,
    MatSlideToggleModule,
    MatPaginatorModule,
    JsonPipe,
    MatTabsModule,
    MatButtonModule, 
    MatDividerModule, 
    MatIconModule],
})



export class TablePaginationExampleComponent implements AfterViewInit  {
  pageEvent!: PageEvent;
  length:number = 1281;
  pageSize:number = 10;
  pageIndex:number = 0;
  pageSizeOptions = [5, 10, 25];

  pageEventPais!: PageEvent;
  lengthPais:number = 246;
  pageSizePais:number = 10;
  pageIndexPais:number = 0;
  pageSizeOptionsPais = [5, 10, 25];


  hidePageSize = false;
  showPageSizeOptions = true;
  showFirstLastButtons = true;
  disabled = false;

  hidePageSizePais = false;
  showPageSizeOptionsPais = true;
  showFirstLastButtonsPais = true;
  disabledPais = false;
  
  displayedColumns: string[] = ['name'];
  displayedColumnsPaises: string[] = ['codPais','nombrePais'];
  
  dataSource = new MatTableDataSource<Pokemon>();
  dataSourcePais = new MatTableDataSource<any>();

  formularioIngreso:any;
  formularioIngresoPais:any;
  modeloAutor:Autor;
  modeloRespuesta:AutorRespuesta;

  ingresoConversion:string="";
  respuestaConversion:string="";

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatPaginator) paginatorPais!: MatPaginator;

  ngAfterViewInit() {
    this.getPokemons();
    this.getDatosSoapCountry();
    
  }

  constructor(private servicePokemon:PokemonService){
    


   this.modeloAutor = {
    title:"",
    body:"",
    userId:1
   }; 
   this.modeloRespuesta={
    title:"",
    body:"",
    userId:0,
    id:0
   }
  }

  handlePageEvent(e: PageEvent) {
    this.pageEvent = e;
    this.length = e.length;
    this.pageSize = e.pageSize;
    this.pageIndex = e.pageIndex;
  }

  handlePageEventPais(e: PageEvent) {
    this.pageEventPais = e;
    this.lengthPais = e.length;
    this.pageSizePais = e.pageSize;
    this.pageIndexPais = e.pageIndex;
  }

  setAutor(){
    console.log(this.modeloAutor)
    this.servicePokemon.setUsuario(this.modeloAutor).subscribe(
      data =>{
        console.log(data)
        this.modeloRespuesta = data
      }
    )
  }

  setConversion(){
    this.servicePokemon.setDatosCountySoap(this.ingresoConversion).
    subscribe(
      data =>{
        console.log(data);
      }
    );
  }

  getDatosSoapCountry(){
    this.servicePokemon.getDatosCountySoap().subscribe(data => {
      console.log(data);
      this.dataSourcePais.data = this.convertXMLtoJSON(data);
      this.dataSourcePais.paginator = this.paginatorPais;
      this.respuestaConversion = this.convertXMLtoJSONNumberToWords(data);

    });
  }

  getPokemons(){
    this.servicePokemon.getDatosPokemon("0", "1281").
    subscribe(
      data =>{
        //console.log(data.body.results);
        pokemons = data.body.results;
        console.log(pokemons);
        this.dataSource.data = data.body.results;
        this.dataSource.paginator = this.paginator;
      }
    )
  }

  //funcion que convierte xml a json
  convertXMLtoJSON(xmltxt:string){
    // Create the return object
    var obj = {};
    let xml = (new DOMParser()).parseFromString(xmltxt, "text/xml");
    //[0].getElementsByTagName("m:sISOCode")[0].childNodes[0].nodeValue
    console.log(xml.getElementsByTagName("m:tCountryCodeAndName").length);
     //modelo de mi front
    
    let arregloPaises= [];
    for(let x = 0 ; x < xml.getElementsByTagName("m:tCountryCodeAndName").length ; x++){
      let paisSoap= {
        nombrePais: xml.getElementsByTagName("m:tCountryCodeAndName")[x].getElementsByTagName("m:sName")[0].childNodes[0].nodeValue,
        codPais: xml.getElementsByTagName("m:tCountryCodeAndName")[x].getElementsByTagName("m:sISOCode")[0].childNodes[0].nodeValue
      };
      arregloPaises.push(paisSoap);

    }

    return arregloPaises;
  }

  //funcion que convierte xml a json
  convertXMLtoJSONNumberToWords(xmltxt:string):string{
    // Create the return object
    var obj = {};
    let xml = (new DOMParser()).parseFromString(xmltxt, "text/xml");
    //[0].getElementsByTagName("m:sISOCode")[0].childNodes[0].nodeValue
    console.log(xml.getElementsByTagName("m:tCountryCodeAndName").length);
     //modelo de mi front
    let obtenerNNumberword:string = xml.getElementsByTagName("m:NumberToWordsResponse")[0].getElementsByTagName("m:NumberToWordsResult")[0].childNodes[0].nodeValue?.toString()
   
    return obtenerNNumberword;
  }
}


export interface Pais {
  codPais: string;
  nombrePais: string;
}

let pokemons:Pokemon[] = [];

