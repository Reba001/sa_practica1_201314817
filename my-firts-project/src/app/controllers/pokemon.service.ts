import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Servicio } from '../models/servicio';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Autor } from '../models/autor';

const url = 'https://pokeapi.co/api/v2/pokemon';
const urlJsonPlaceHolder = 'https://jsonplaceholder.typicode.com/posts';

@Injectable({
  providedIn: 'root'
})
export class PokemonService {
  httpOptionsSoap ={
    headers: new HttpHeaders({'Content-Type':'text/xml;charset=utf-8'})
  }; 
  constructor(private httpClient: HttpClient) { }

  getDatosPokemon(offset:string, limit:string):Observable<any>{
    let urlCompleta = `${url}?offset=${offset}&limit=${limit}`;
    console.log(urlCompleta);
    return this.httpClient.get<any>(urlCompleta,{ observe: 'response' } );
  }

  setUsuario(autor:Autor){
    return this.httpClient.post<any>(urlJsonPlaceHolder, autor );
  }

  getDatosCountySoap(){
    const xml = `<?xml version="1.0" encoding="utf-8"?>
    <soap12:Envelope xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">
      <soap12:Body>
        <ListOfCountryNamesByName xmlns="http://www.oorsprong.org/websamples.countryinfo">
        </ListOfCountryNamesByName>
      </soap12:Body>
    </soap12:Envelope>`;
    return this.httpClient.post('http://webservices.oorsprong.org/websamples.countryinfo/CountryInfoService.wso', xml,{headers:{'Content-Type':'text/xml;charset=utf-8'}, responseType:"text"});
  }

  setDatosCountySoap(numberConversion:string){
    const xml = `<?xml version="1.0" encoding="utf-8"?>
    <soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
      <soap:Body>
        <NumberToWords xmlns="http://www.dataaccess.com/webservicesserver/">
          <ubiNum>${numberConversion}</ubiNum>
        </NumberToWords>
      </soap:Body>
    </soap:Envelope>`;
    return this.httpClient.post('https://www.dataaccess.com/webservicesserver/NumberConversion.wso', xml,{headers:{'Content-Type':'text/xml;charset=utf-8'}, responseType:"text"});
  }
}
