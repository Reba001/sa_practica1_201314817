export interface Autor {
    title:string;
    body:string;
    userId:number;
}

export interface AutorRespuesta {
    title:string;
    body:string;
    userId:number;
    id:number;
}
