import { Pokemon } from "./pokemon";

export interface Servicio {
    count:number;
    next:string;
    previous:string;
    results:Pokemon[];
}
