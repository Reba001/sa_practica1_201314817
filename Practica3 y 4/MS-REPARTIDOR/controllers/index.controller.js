
let listaPedidos = [];

const getHola = async(req, res)=>{
    res.status(200).send({status:200, msg:"hola mundo mi microservicio restaurante", data:[]});
}


// ingresa los datos que se piden al restaurante y lo devuelve
const setPedidoDelRestaurante = async(req, res)=>{
    try{
        const idPedido = listaPedidos.length + 1;
        listaPedidos.push({idCliente: req.body.idCliente, comida: req.body.comida, idPedido, idEstado: 1, estado:"ingresado"});
        res.status(201).json({status:201, msg:`Pedido ingresado correctamente, numero de pedido: ${idPedido}!, estado: ingresado`, data:[]});


    }catch(ex){
        console.log(ex.message);
        res.status(500).json({status:500, msg:"Error en la operacion", data:[ex]});
    }

}

// verifica el estado de restaurante

const getEstadoPedidoCliente = async(req,res)=>{
    try{
        const estadoPedido = listaPedidos.find(x => x.idPedido == req.params.idPedido);
        listaPedidos = listaPedidos.filter(x => x.idPedido == req.params.idPedido);
        estadoPedido.estado = "El repartidor esta frente a su puerta";
        estadoPedido.idEstado = 6;
        listaPedidos.push(estadoPedido);
        res.status(201).json({status:201, msg:`Su estado de pedido es el  ${estadoPedido.estado}`, data:[]});


    }catch(ex){
        console.log(ex.message);
        res.status(500).json({status:500, msg:"Error en la operacion", data:[ex]});
    }
}


// verifica el estado de restaurante

const getEstadoPedidoEntregado = async(req,res)=>{
    try{
        const estadoPedido = listaPedidos.find(x => x.idPedido == req.params.idPedido);
        listaPedidos = listaPedidos.filter(x => x.idPedido == req.params.idPedido);
        estadoPedido.estado = "Entregado";
        estadoPedido.idEstado = 7;
        listaPedidos.push(estadoPedido);
        res.status(201).json({status:201, msg:`Su estado de pedido es el  ${estadoPedido.estado}`, data:[]});


    }catch(ex){
        console.log(ex.message);
        res.status(500).json({status:500, msg:"Error en la operacion", data:[ex]});
    }
}

module.exports ={
    getHola,setPedidoDelRestaurante,getEstadoPedidoCliente, getEstadoPedidoEntregado
}