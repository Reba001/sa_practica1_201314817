const express = require('express');
const router = express.Router();

const {getHola, setPedidoRestaurante, getEstadoPedido, getEstadoPedidoRepartidor} = require('../controllers/index.controller');


router.get('/hola', getHola);
router.post('/set-pedido', setPedidoRestaurante);
router.get('/estado-pedido/:idPedido', getEstadoPedido );
router.get('/estado-pedido-repartidor/:idPedido', getEstadoPedidoRepartidor );

router.use((err, req, res, next)=>{
    res.status(500).send({status:500, msg:'Error con la operacion!', data:[]});
    console.log(`${err.status || 500} - ${res.statusMessage} - ${err.message} - ${req.originalUrl} - ${req.method} - ${req.ip}`);
});
  
router.use((req, res, next)=>{
    res.status(404).send({status:404, msg:'Pagina no encontrada', data:[]});
    console.log(`404 || ${res.statusMessage} - ${req.originalUrl} - ${req.method} - ${req.ip}`);
});

module.exports = router;