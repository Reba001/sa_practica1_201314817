const createCommonMiddleware = require('./common');

const aplyMiddleware = (app)=>{
    createCommonMiddleware(app);
}

module.exports = aplyMiddleware;