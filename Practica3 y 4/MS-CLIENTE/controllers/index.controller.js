
let listaPedidos = [];

const getHola = async(req, res)=>{
    res.status(200).send({status:200, msg:"hola mundo mi microservicio", data:[]});
}


// ingresa los datos que se piden al restaurante y lo devuelve
const setPedidoRestaurante = async(req, res)=>{
    try{
        const idPedido = listaPedidos.length + 1;
        listaPedidos.push({idCliente: req.body.idCliente, comida: req.body.comida, idPedido, idEstado: 1, estado:"ingresado"});
        res.status(201).json({status:201, msg:`Pedido ingresado correctamente, numero de pedido: ${idPedido}!, estado: ingresado`, data:[]});


    }catch(ex){
        console.log(ex.message);
        res.status(500).json({status:500, msg:"Error en la operacion", data:[ex]});
    }

}

// verifica el estado de restaurante

const getEstadoPedido = async(req,res)=>{
    try{
        const estadoPedido = listaPedidos.find(x => x.idPedido == req.params.idPedido);
        listaPedidos = listaPedidos.filter(x => x.idPedido == req.params.idPedido);
        estadoPedido.estado = "Enviado a cliente";
        estadoPedido.idEstado = 2;
        listaPedidos.push(estadoPedido);
        res.status(201).json({status:201, msg:`Su estado de pedido es el  ${estadoPedido.estado}`, data:[]});


    }catch(ex){
        console.log(ex.message);
        res.status(500).json({status:500, msg:"Error en la operacion", data:[ex]});
    }
}


// verifica el estado de restaurante

const getEstadoPedidoRepartidor = async(req,res)=>{
    try{
        const estadoPedido = listaPedidos.find(x => x.idPedido == req.params.idPedido);
        listaPedidos = listaPedidos.filter(x => x.idPedido == req.params.idPedido);
        estadoPedido.estado = "En camino";
        estadoPedido.idEstado = 3;
        listaPedidos.push(estadoPedido);
        res.status(201).json({status:201, msg:`Su estado de pedido es el  ${estadoPedido.estado}`, data:[]});


    }catch(ex){
        console.log(ex.message);
        res.status(500).json({status:500, msg:"Error en la operacion", data:[ex]});
    }
}

module.exports ={
    getHola,setPedidoRestaurante,getEstadoPedido, getEstadoPedidoRepartidor
}