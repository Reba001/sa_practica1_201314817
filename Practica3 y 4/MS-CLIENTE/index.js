const express = require('express');
const PORT = 3000;
const middleware = require('./middlewares');

const createApp = ()=>{
    const app = express();
    middleware(app);
    app.use('/cliente', require('./routes/index.js'));
    
    return app;
}

const app = createApp();

const server = app.listen(PORT, ()=>{
    console.log(`Api ejecutandose en la dirección: http://localhost:${PORT}`);
})

module.exports ={
    app,
    server
}